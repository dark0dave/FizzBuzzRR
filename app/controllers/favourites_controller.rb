require 'will_paginate/array'

class FavouritesController < ApplicationController
  before_action :set_favourite, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token
  require 'fizzbuzz'

  # Set defaults for fizzBuzz
  RESUTLSPERPAGE = 100
  HIGHESTNUMBER = 100000000000
  # GET /favourites
  # GET /favourites.json
  def index
    @favourites = Favourite.all
    fizzBuzzObj = FizzBuzz.new
    # Create a hash so favourites can be loaded by view
    @FavouriteFizz = {}
    @favourites.each do |favourite|
      @FavouriteFizz[favourite.fnumber] = favourite
    end

    # Parameterized loading or default settings
    numberToStartAt   = favourite_params[:starts_at].to_i()
    resultsPerPage    = (favourite_params[:per_page]  || RESUTLSPERPAGE).to_i()
    pageNumber        = (favourite_params[:page]      || numberToStartAt/resultsPerPage + 1 ).to_i() -1
    numberToEndAt     = (favourite_params[:end_at]    || HIGHESTNUMBER).to_i()

    # Page by page
    numberToStartAtOnPage   = pageNumber * resultsPerPage
    numberToEndAtOnPage     = pageNumber * resultsPerPage + resultsPerPage
    # numberToEndAtOnPage must be less than end_at so requests don't fetch more than they need too
    numberToEndAtOnPage = numberToEndAtOnPage > numberToEndAt ? numberToEndAt : numberToEndAtOnPage
    numberToStartAtOnPage = numberToStartAtOnPage > numberToStartAt ? numberToStartAtOnPage : numberToStartAt

    fizzBuzzArr = fizzBuzzObj
      .fizzBuzzArr( numberToStartAtOnPage , numberToEndAtOnPage -1 )

    @fizzbuzz = fizzBuzzArr.paginate(
      :page => params[:page],
      :per_page => resultsPerPage,
      :total_entries => numberToEndAt/resultsPerPage
      ).replace(fizzBuzzArr)
  end

  # POST /favourites
  # POST /favourites.json
  def create
    new_favourite_number = favourite_params[:numberToFavourite]
    @favourite = Favourite.new(fnumber:new_favourite_number)
    @favourite.save
    redirect_to_back
  end

  # DELETE /favourites/1
  # DELETE /favourites/1.json
  def destroy
    @favourite.destroy
    redirect_to_back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favourite
      @favourite = Favourite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favourite_params
      params.permit(:fnumber,:numberToFavourite,:page,:end_at,:per_page,:starts_at)
    end

    def redirect_to_back(default = root_url)
      if request.env["HTTP_REFERER"].present? && request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]
        redirect_to :back
      else
        redirect_to default
      end
    end

end
