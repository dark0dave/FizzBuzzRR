class Api::V1::FizzbuzzApiController < ApplicationController
  require 'fizzbuzz'

  # Set defaults for fizzBuzz
  RESUTLSPERPAGE = 100
  HIGHESTNUMBER = 100_000_000_000
  def show
    fizzBuzzObj = FizzBuzz.new

    # Parameterized loading or default settings
    numberToStartAt   = fizzbuzz_params[:starts_at].to_i()
    resultsPerPage    = (fizzbuzz_params[:per_page]  || RESUTLSPERPAGE).to_i()
    pageNumber        = (fizzbuzz_params[:page]      || numberToStartAt/resultsPerPage + 1 ).to_i() -1
    numberToEndAt     = (fizzbuzz_params[:end_at]    || HIGHESTNUMBER).to_i()

    # Page by page
    numberToStartAtOnPage   = pageNumber * resultsPerPage
    numberToEndAtOnPage     = pageNumber * resultsPerPage + resultsPerPage
    # numberToEndAtOnPage must be less than end_at so requests don't fetch more than they need too
    numberToEndAtOnPage = numberToEndAtOnPage > numberToEndAt ? numberToEndAt : numberToEndAtOnPage
    numberToStartAtOnPage = numberToStartAtOnPage > numberToStartAt ? numberToStartAtOnPage : numberToStartAt

    fizzBuzzArr = fizzBuzzObj
      .fizzBuzzArr( numberToStartAtOnPage , numberToEndAtOnPage -1 )

    render json:fizzBuzzArr, status: 200

  end

  private
  def fizzbuzz_params
    params.permit(:page,:end_at,:starts_at,:per_page)
  end
end
