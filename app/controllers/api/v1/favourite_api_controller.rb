class Api::V1::FavouriteApiController < ApplicationController

  def create

    @favourite = Favourite.new(fnumber:favourite_params[:fnumber])

    if !@favourite.nil?
      @favourite.save
      render json: @favourite.to_json, status:200
    else
      render json: {error:"Favourite could not be created"} , status:422
    end

  end

  def destroy

    @favourite = Favourite.all.find_by(fnumber:favourite_params[:fnumber])

    if !@favourite.nil?
      @favourite.destroy
      render json: {sucesss:"Favourite number #{favourite_params[:fnumber]} deleted" }, status: 200
    else
      render json: {error:"Favourite could not be deleted"} , status:422
    end

  end

  def show

    @favourite = Favourite.where(fnumber:favourite_params[:fnumber])

    render json: @favourite.to_json, status:200

  end

  def index

    render json: Favourite.all.to_json, status:200

  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def favourite_params
    params.permit(:fnumber)
  end

end
