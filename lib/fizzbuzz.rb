class FizzBuzz

  def isFizzBuzz? (number)

    result = ""

    if number.modulo(3).zero?
      result << "fizz"
    end

    if number.modulo(5).zero?
      result << "buzz"
    end

    number.zero? || result.length < 1 ? number.to_s : result

  end

  def fizzBuzzArr (startsAt, endsAt)

    fizzBuzzArr = []

    Range.new(startsAt,endsAt).to_a.map do |number|
      fizzBuzzArr.push([number,isFizzBuzz?(number)])
    end

    fizzBuzzArr
  end

end
