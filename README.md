# Fizz Buzz App

This is fizzbuzz application (written ruby on rails), which lists all the numbers from 0 to 100,000,000,000 and sees if they are divisable by 3 or 5 and returns fizz,buzz or fizzbuzz accordingly. It also allows you to set favourites which are persisted in a backend in postgress (sqlite in testing) database. Further more there is a restful api which allows the above functionality as well.

## Detailed description


### Task A: Ruby on Rails FizzBuzz application
- Standard FizzBuzz rules, divisible by 3 is Fizz, divisible by 5 is Buzz
- Should display values from 1 to 100 on the homepage
- Should allow viewing values up to 100,000,000,000
- Should have pagination
- Should allow changing of the page size
- User should be able to mark certain numbers as their favourites, these should indicate that they are favourites on the UI and be persisted
- Should provide a JSON API with all the above-mentioned functionality

### Task B: Client to consume the JSON API

- Create a client to consume the FizzBuzz application API
- Should be available from the command line
- Should be written in a language other than Ruby

## App

- Ruby version : 2.3.1

- Rails version : 5.0.0.1

- Database creation : postgress

- Dependencies : wil_pagniate

- Configuration : Must have docker-compose installed then run the runme.sh files

- Navigate to [http://localhost:3000/]
- To change page size see: [http://localhost:3000/favourites?page=2&per_page=1000]

### Favourite API

- [http://localhost:3000/api/v1/favourite_api/index]
- shows all current favourites
- [http://localhost:3000/api/v1/favourite_api/show?fnumber=7]
- searches through favourites 7 in our case
- [http://localhost:3000/api/v1/favourite_api/destroy?fnumber=7]
- deletes favourite 7
- [http://localhost:3000/api/v1/favourite_api/create?fnumber=7]
- creates favourite 7

### Fizzbuzz API

- [http://localhost:3000/api/v1/fizzbuzz_api/show]
- Shows the default fizz buzz from 0 100
- [http://localhost:3000/api/v1/fizzbuzz_api/show?end_at=90000000010&starts_at=90000000000]
- Shows up to the 100,000,000,000
- You are restricted to 10,000 per request and must enter start and end number
- Can also call it with the page and per_page parameters
- [http://localhost:3000/api/v1/fizzbuzz_api/show?page=2&per_page=1000]

## Client 

- Is located in node-client/ to build the dependencies for the project run
```
npm install
```
- To run Client simply enter one of the api addresses above
```
node client.js http://localhost:3000/api/v1/favourite_api/create?fnumber=7
```

## Test suite 
- Using rpsec, to test the fizzbuzz functionality run
```
rspec spec/lib/fizzbuzz_spec.rb
```
