Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      get 'favourite_api/create'
      get 'favourite_api/destroy'
      get 'favourite_api/show'
      get 'favourite_api/index'

      get 'fizzbuzz_api/show'
    end
  end

  resources :favourites
  root to: 'favourites#index'

end
