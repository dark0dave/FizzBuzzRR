require 'rails_helper'

RSpec.describe Api::V1::FizzbuzzApiController, type: :controller do

  let(:valid_params) {
    { end_at:5 }
  }

  describe "GET #show" do
    it "returns an expected result for end_at" do
      expected_result =[[0,"0"],[1,"1"],[2,"2"],[3,"fizz"],[4,"4"]]

      get "show" , params: {"end_at": 5}

      json = JSON.parse(response.body)

      # # test for the 200 status-code
      expect(response).to be_success

      # check to make sure the right amount of messages are returned
      expect(json).to eq(expected_result)

    end

    it "returns an expected result for starts_at and end_at" do
      expected_result =[[1,"1"],[2,"2"],[3,"fizz"],[4,"4"]]

      get "show" , params: {"end_at": 5,"starts_at": 1}

      json = JSON.parse(response.body)

      # # test for the 200 status-code
      expect(response).to be_success

      # check to make sure the right amount of messages are returned
      expect(json).to eq(expected_result)

    end

    it "returns an expected result for starts_at and end_at and page" do
      expected_result =[[595, "buzz"], [596, "596"], [597, "fizz"], [598, "598"], [599, "599"]]

      get "show" , params: {"starts_at": 595,"end_at": 600}

      json = JSON.parse(response.body)
      # # test for the 200 status-code
      expect(response).to be_success

      # check to make sure the right amount of messages are returned
      expect(json).to eq(expected_result)

    end

  end


end
