require 'rails_helper'

RSpec.describe Api::V1::FavouriteApiController, type: :controller do

  let(:valid_attributes) {
    { id: 1, fnumber: 10, created_at: "2016-10-22 13:53:05.998764", updated_at: "2016-10-22 13:53:05.998764" }
  }

  let(:invalid_attributes) {
    { id: "hat", fnumber: "hat", created_at: "2016-10-22 13:53:05.998764", updated_at: "2016-10-22 13:53:05.998764" }
  }

  describe "GET #create" do
    it "returns http success" do
      favourite = Favourite.create! valid_attributes
      expect {
        post :create, params: {fnumber: favourite.fnumber}
      }.to change(Favourite, :count).by(1)
      expect(response).to have_http_status(:success)
      expect(Favourite.where(fnumber: favourite.fnumber)).to exist
    end
  end

  describe "GET #destroy" do
    it "returns http success" do
      favourite = Favourite.create! valid_attributes
      expect {
        delete :destroy, params: {fnumber: favourite.fnumber}
      }.to change(Favourite, :count).by(-1)
      expect(response).to have_http_status(:success)
      expect(Favourite.where(fnumber: favourite.fnumber)).not_to exist
    end
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      get :show
      expect(response).to have_http_status(:success)
    end
  end

end
