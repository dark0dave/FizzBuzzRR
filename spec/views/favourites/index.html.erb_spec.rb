require 'rails_helper'

RSpec.describe "favourites/index", type: :view do

  it "renders a display values from 1 to 100 (plus headers)" do
    assign(:fizzbuzz, (1..100))
    assign(:FavouriteFizz, [])
    render
    assert_select "tr>td", :text => "".to_s, :count => 100
  end

  it "renders large number of values from user input" do
    end_at = 10000
    assign(:fizzbuzz, (1..end_at))
    assign(:FavouriteFizz, [])
    render
    assert_select "tr>td", :text => "".to_s, :count => end_at
  end

  it "renders values up to  100,000,000,000" do
    end_at = 100_000_000_000
    starts_at = 99_999_999_901
    assign(:fizzbuzz, (starts_at..end_at))
    assign(:FavouriteFizz, [])
    render
    assert_select "tr>td", :text => "".to_s, :max => 100_000_000_000
  end


end
