require 'rails_helper'
require 'fizzbuzz'

RSpec.describe FizzBuzz do

  context "should give fizz, buzz or fizzbuzz when given a number" do

    it "returns fizz when the number is divisable by 3" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.isFizzBuzz?(3)).to eq "fizz"
    end

    it "returns buzz when the number is divisable by 5" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.isFizzBuzz?(5)).to eq "buzz"
    end

    it "returns fizzbuzz when the number is divisable by 3 and 5" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.isFizzBuzz?(15)).to eq "fizzbuzz"
    end

    it "returns an empty string when the number is 0" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.isFizzBuzz?(0)).to eq "0"
    end

    it "returns an empty string when the number is not divisable by 3 or 5" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.isFizzBuzz?(7)).to eq "7"
    end
  end

  context "generates a range of fizz buzzs" do

    it "should contain empty values when number not divisable by 3 or 5" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.fizzBuzzArr(0,2)).to match_array([[0, "0"], [1, "1"], [2, "2"]])
    end

    it "should contain fizz,buzz or FizzBuzz when number is divisable by 3 or 5" do
      fizzBuzz = FizzBuzz.new
      expect(fizzBuzz.fizzBuzzArr(3,5)).to match_array([[3, "fizz"], [4, "4"], [5, "buzz"]])
      expect(fizzBuzz.fizzBuzzArr(12,15)).to match_array([[12, "fizz"], [13, "13"], [14, "14"], [15, "fizzbuzz"]])
    end
  end

end
