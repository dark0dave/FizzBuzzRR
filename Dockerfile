# Get latest ruby image
FROM ruby:latest

# Install our dependencies for javascript
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

# Create working dir for App
RUN mkdir /myapp
WORKDIR /myapp

# Add Gems plus fizzbuzz lib
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
ADD lib/fizzbuzz.rb /myapp/lib/.

# Install gems
RUN bundle install
ADD . /myapp
