var request = require("request");

var params = [];

checkIfArgPopulated()

var queryURI = params.toString()

console.log(queryURI)

query(queryURI)

function checkIfArgPopulated () {
  if (process.argv.length > 2) {
    process.argv.forEach(function (val, index) {
      if (index >1) {
        params.push(val);
      }
    })
   }
  else {
    console.log("Please provide argument <URI>")
    console.log("Example: node client.js http://localhost:3000/api/v1/favourite_api/show?fnumber=7")
    process.exit(1);
  }
}

function query (queryURI) {
  request(queryURI , function(error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body);
      }
      else {
        console.log(error);
      }
  });
}
